package Main;


import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Main.PaintPanel.moveCircle;

public class Main extends JFrame 
{
	public static Main instance = null;
	PaintPanel panel;
	JButton addButt = new JButton("Add Circle");
	
	JTextField sizeX = new JTextField();
	JTextField sizeY = new JTextField();
	JTextField sizeR = new JTextField();
	JLabel label = new JLabel("Enter dimensions:");
	
	
	Container container = this.getContentPane();
	
	private Main()
	{
		initComponents();
	}
	
	private void initComponents()
	{
		this.setDefaultCloseOperation(3);
		panel = new PaintPanel();
		this.setVisible(true);
		this.setBounds(100,100,1600,900);
		this.setLayout(null);
		this.getContentPane().add(panel);
		
		addButt.setBounds(768, 800, 100, 40);
		addButt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				panel.createAndDrawCircle(Integer.parseInt(sizeX.getText()),Integer.parseInt(sizeY.getText()),Integer.parseInt(sizeR.getText()));
				//System.out.println(panel.circles.get(0).circle.getX() + " " + panel.circles.get(0).circle.getY() + " " + panel.circles.get(0).circle.getR());
			}
			
		});
		this.getContentPane().add(addButt);
		
		label.setBounds(50, 690, 250, 40);
		this.getContentPane().add(label);
		
		sizeX.setBounds(50,720,300,40);
		this.getContentPane().add(sizeX);
		
		sizeY.setBounds(50,760,300,40);
		this.getContentPane().add(sizeY);
		
		sizeR.setBounds(50,800,300,40);
		this.getContentPane().add(sizeR);



		
	}
	
	public static Main getInstance()
	{
		if(instance == null) instance = new Main();
		return instance;
	}
	
	public static void main(String[] args)
	{
		Main.getInstance();
	}

}
