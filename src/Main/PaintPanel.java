package Main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JPanel;

public class PaintPanel extends JPanel implements KeyListener
{
	public ArrayList<moveCircle> circles = new ArrayList<moveCircle>();
	
	int px = 0;
	int py = 0;
	
	
	public PaintPanel()
	{
		super();
		this.setBounds(0, 0, 1600, 700);
		this.setBackground(Color.WHITE);
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_UP)
		{
			/*
			 * 
			 * 
			 * 
			 */
		}
		else if(e.getKeyCode() == KeyEvent.VK_DOWN)
		{
			/*
			 * 
			 * 
			 * 
			 */
		}
		else if(e.getKeyCode() == KeyEvent.VK_LEFT)
		{
			/*
			 *
			 * 
			 * 
			 */
		}
		else if(e.getKeyCode() == KeyEvent.VK_RIGHT)
		{
			/*
			 * 
			 * 
			 * 
			 */
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	void createAndDrawCircle(int x, int y, int r)
	{
	  moveCircle mCircle = new moveCircle(x,y,r); 
	  circles.add(mCircle);
	  draw(this.getGraphics(),mCircle.circle);
	}
	public void draw(Graphics graphics, Circle circle)
	{
		graphics.setColor(Color.black);
		graphics.fillOval(circle.getX(), circle.getY(), circle.getR(), circle.getR());
	}
	
	
	
	class moveCircle implements Runnable
	{
		Circle circle;
		
		
		moveCircle(int x, int y, int r)
		{
			circle = new Circle(x,y,r);
		}
		
		@Override
		public void run() {
			while(true)
			{
				System.out.println("Cos");
				try {
					wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}
	
	class Circle
	{
		private int x;
		private int y;
		private int r;
		
		Circle(int x, int y, int r)
		{
			this.x = x;
			this.y = y;
			this.r = r;
		}
		
		public int getX()
		{
			return x;
		}
		public int getY()
		{
			return y;
		}
		public int getR()
		{
			return r;
		}

		
		
	}

	

}
